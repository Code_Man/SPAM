
local basegame = "rapid://s44:test"

local modinfo = {
	name = "SPAM + " .. basegame,
	shortname = "SPAM + " .. basegame,
	game = "SPAM  + " .. basegame,
	shortgame = "SPAM  + " .. basegame,
        version = "0.1 Beta - Dev",
	description = "SPAM game mode",
	url = "http://code_man.cybnet.ch/projects",
	modtype = 1,
        mutator = 1,

	depend = {
		basegame,
	}
}

return modinfo
