
-- resources.lua

-- Spam resource file

local resources = {
    graphics = {
        projectiletextures = {
            spam_missile_trail = 'spam_missile_trail.tga',
	}
    }
}

return resources
