
-- 

-- 

local body = piece "body"
local antennas = piece "antennas"

local turret = piece "turret"
local barrel = piece "barrel"

local turret2 = piece "barrel2"

local turret_pitch

local turret_idle_angle = 175
local max_idle_delay = 12500
local turret_turn_speed = 0.5
local barrel_turn_speed = 0.25

local function turn_local (part, axis, c, speed) -- shitty thread handling workaround
    Turn (part, axis, c, speed)
end

local function move_local (part, axis, c, speed) -- shitty thread handling workaround
    Move (part, axis, c, speed)
end

local function start_move ()
    SetSignalMask (2)
    Turn (antennas, 1, -0.20, 0.5)
    Turn (body, 1, -0.03, 0.25)
    WaitForTurn (body, 1)
    Turn (body, 1, 0, 0.25)
end

function script.StartMoving ()
    Signal (1)
    StartThread (start_move)
end

local function stop_move ()
    SetSignalMask (1)
    Turn (antennas, 1, 0, 0.5)
    Turn (body, 1, 0.03, 0.25)
    WaitForTurn (body, 1)
    Turn (body, 1, 0, 0.25)
end

function script.StopMoving ()
    Signal (2)
    StartThread (stop_move)
end

local function return_turret ()
    Sleep (2500)
    Turn (turret, 2, 0, turret_turn_speed)
    Turn (barrel, 1, 0, barrel_turn_speed)
    WaitForTurn (turret, 2)
    WaitForTurn (barrel, 1)
end

local function idle_anim ()
    SetSignalMask (1)
    return_turret ()
    while true do
        Sleep (math.random (1500, max_idle_delay))
        Turn (turret, 2, math.random (0, turret_idle_angle) / 100 - turret_idle_angle / 100 / 2, turret_turn_speed)
    end
end

local function turn_to_target (heading)
    SetSignalMask (3)
end

local function display_muzzle ()
    --Show(piece "muzzle")
    Sleep(10)
    --Hide(piece "muzzle")
end

function script.AimFromWeapon1 ()
    return turret
end

function script.QueryWeapon1 ()
    return barrel
end

function script.AimWeapon1 (heading, pitch)
    if (pitch > 0.45) or (pitch < -0.1) then
        return false
    end
    Signal (1)
    turret_pitch = pitch
    Turn (turret, 2, heading, turret_turn_speed)
    Turn (barrel, 1, -pitch + 0.05, barrel_turn_speed) -- Aiming a bit lower than pitch looks better ingame
    WaitForTurn (turret, 2)
    WaitForTurn (barrel, 1)
    StartThread (idle_anim)
    return true
end

function script.FireWeapon1 ()
    StartThread (display_muzzle)
    Move (barrel, 3, -5.0, 150)
    --Move(barrel, 1, math.sin(turret_pitch * 1000) / 10, 150)
    WaitForMove(barrel, 3)
    Move (barrel, 3, 0, 100)
    Move (barrel, 1, 0, 100)
end

function script.Create ()
--    StartThread (idle_anim)
end

function script.Killed (recentDamage, maxHealth)
    if (recentDamage >= 100) then
         Turn (turret, 3, 2, 4)
         Move (turret, 2, 2, 4)
         Sleep (1000)
    end
    Signal (1)
    Move (piece "body", 2, -30, 2)
    WaitForMove (piece "body", 2)
    return 0
end

function script.QueryWeapon2 ()
    return turret2
end

function script.AimFromWeapon2 ()
    return turret2
end

function script.AimWeapon2 (heading, pitch)
    Turn (turret2, 2, heading, 0.5)
    WaitForTurn (turret2, 2)
    Turn (turret2, 1, -pitch, 0.3)
    WaitForTurn (turret2, 1)
    return true
end

function script.FireWeapon2 ()
end
