SPAM: Subsequent permanent assault mode

A drop in AI mutator for games on the spring engine.
This gadget aims to be a configurable and universal tool for game developers primarely, but that can also be used by players.

SPAM can be either directly embeded in a game or used as a mutator, here is how.

1 Embeding directly:
To include spam in your game, first you need to do is to copy the LuaRules folder in your game and most probably edit the configuration file.
The configuration file is in LuaRules/Configs/spam_config.lua, all fields are commented, you are not strictly required to edit it.
However in that case the predefined spam units will be used with default values.
An important part is to make sure a moveDef named default exists in your game, if you chose this path or the engine will crash.
The files in units, unittextures, objects3d, scripts will also have to be copied if you want to use spam defaults.
If you want to use game specific units you must specify them in the configuration file.
Note however that you must have a unit called "spam_hq", this unit must be a factory and have all the buildoptions of the units you want to use.
One is provided by default naturally but you can make your own with its own theme.
You must also edit luaAI.lua to include spam, or it will not show up.
These limitations are part of the engine and for now have to remain as is, try to make it the easiest way possible to use but also flexible.

2 Using as a mutator:
This method has the advantage you do not need to edit any files for the game you wish to play it with.
Just like in method one you can or can not edit the configuration file at your leasure.
However for this to work you must edit the modinfo.lua and set the variable named "basegame".
It can use a rapid tag to be always based on the newest version or if you prefer a specific version.
Another restriction is that other lua ais will not be usable unless you edit luaAI.lua