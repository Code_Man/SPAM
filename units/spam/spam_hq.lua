
-- spam_hq.lua

--

local unitName = "spam_hq"

local unitDef = {
    name = "Spammer HQ",
    description = "Spams units",
    category = "STRUCTURE SMALL NOTAIR NOTSUB",
    objectName = "weapons_factory.dae",
    maxDamage = 1000000,
    footprintX = 10,
    footprintZ = 10,
    collisionVolumeType = "box",
    collisionVolumeScales = {150, 75, 150},
    script = "airstrip.lua",
    iconType = "building",

    sightDistance = 1000,

    buildPic = "spam_hq.png",
    buildTime = 10000,
    buildCostMetal = 100000,

    yardmap = "ooooo ooooo\
               ooooo ooooo\
               ooooo ooooo\
               ooooo ooooo\
               ooooo ooooo\
               ccccc ccccc\
               ccccc ccccc\
               ccccc ccccc\
               ccccc ccccc\
               ccccc ccccc\
               ccccc ccccc",

    maxslope = 20,

    canMove = true, -- Required to set rally point.

    canAttack = true,
    canGuard = true,
    canFight = true,
    canPatrol = true,

    builder = true,
    canRepair = false,
    canBeAssisted = false,
    canAssist = false,
    workerTime = 10.0,
    canRestore = false,
    canReclaim = false,
    showNanoSpray = false,

    buildOptions = {
        "spam_light_tank",
        "spam_heavy_tank",
        "spam_medium_tank",
    },

    MetalMake = 1000,
    MetalStorage = 1000,

    explodeAs = "tank_death",
}

return lowerkeys({ [unitName] = unitDef })
