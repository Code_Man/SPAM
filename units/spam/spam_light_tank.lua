local unitName = "spam_light_tank"

local unitDef = {
    name = "SPAM Light Tank",
    description = "Light Anti-Armor Tank",
    category = "TANK SMALL NOTAIR NOTSUB",
    objectName = "spam_light_tank.dae",
    maxDamage = 200,
    footprintX = 2,
    footprintZ = 2,
    collisionVolumeType = "box",
    collisionVolumeScales = {25, 25, 75},
    --collisionVolumeOffsets = {32, 64, 0},
    script = "spam_light_tank.lua",
    iconType = "tank",

    sightDistance = 400,
    radarDistance = 250,
    
    buildPic = "spam_light_tank.png",
    buildTime = 25,
    buildCostMetal = 250,

    canMove = true,
    leaveTracks = true,
    --TrackType = "tank",
    
    movementClass = "Default",
    acceleration = 0.08,
    brakeRate = 0.1,
    maxSlope = 0.25,
    maxVelocity = 2.5,
    maxReverseVelocity = 1.5,
    maxWaterDepth = 50,
    TurnRate = 350,

    reclaimable = true,
    capturable = true,

    autoHeal = 1.0,

    canAttack = true,
    canGuard = true,
    canFight = true,
    canPatrol = true,

    weapons = {
        [1] = {
            def = "light_laser",
        },
        [2] = {
            def = "rockets",
        },
    },

    sounds = {
        select = {
            "tone16.wav",
        },
        ok = {
            "newtarg1.wav",
        },
        arrived = {
            "tone2.wav",
        },
        cant = {
            "scold2.wav",
        },
    },
    
    explodeAs = "TANKDEATH",
}

local weaponDefs = {
    light_laser = {
	name = "Laser Shotgun",
	weaponType = "LaserCannon",
	avoidFeature = false,
	avoidFriendly = true,
	canAttackGround = true,
	collideFriendly = true,
	noselfDamage = true,
        turret = true,
	areaOfEffect = 2,
        reloadTime = 1.5,
	range = 450,
        targetMoveError = 0.5,
        tolerance = 128,
	--impulsefactor = 1,
	--intensity = 1,
        soundStart = "obelray1",
	beamTime = .1,
	rgbColor = "0.25 0.0 0.5",
        coreThickness = 0.5,
	rgbColor2 = "0.5 0.0 1.0",
	weaponVelocity = 600,
        projectiles = 5,
        sprayAngle = 500,
        --explosionGenerator = "custom:purplelaser",
        laserFlareSize = 1,
        thickness = 1,
        interceptedByShieldType = 1,
	damage = {
            default = 10,--180
        }
    },
    rockets = {
        name = "Laser Beam",
	weaponType = "BeamLaser",
	avoidFeature = false,
	avoidFriendly = true,
	canAttackGround = true,
	collideFriendly = true,
	noselfDamage = true,
        turret = true,
	areaOfEffect = 2,
        reloadTime = 5,
	range = 500,
        targetMoveError = 0.5,
        tolerance = 128,
	--impulsefactor = 1,
	--intensity = 1,
        soundStart = "obelray1",
	beamTime = .1,
	rgbColor = "0.5 0.25 0.0",
        coreThickness = 1,
	rgbColor2 = "0.0 1.0 0.5",
	weaponVelocity = 900,
        --explosionGenerator = "custom:purplelaser",
        laserFlareSize = 2,
        thickness = 2,
        interceptedByShieldType = 1,
        paralyzer = true,
	damage = {
            default = 100,
        }
    },
}

unitDef.weaponDefs = weaponDefs
return lowerkeys({ [unitName] = unitDef })
