local unitName = "spam_medium_tank"

local unitDef = {
    name = "SPAM Medium Tank",
    description = "Medium Anti-Armor Tank",
    category = "TANK SMALL NOTAIR NOTSUB",
    objectName = "spam_medium_tank.dae",
    maxDamage = 250,
    footprintX = 2,
    footprintZ = 2,
    collisionVolumeType = "box",
    collisionVolumeScales = {25, 25, 75},
    --collisionVolumeOffsets = {32, 64, 0},
    script = "spam_light_tank.lua",
    iconType = "tank",

    sightDistance = 400,
    
    buildPic = "spam_medium_tank.png",
    buildTime = 30,
    buildCostMetal = 300,

    canMove = true,
    leaveTracks = true,
    --TrackType = "tank",
    
    movementClass = "Default",
    acceleration = 0.05,
    brakeRate = 0.1,
    maxSlope = 0.20,
    maxVelocity = 2.0,
    maxReverseVelocity = 1.0,
    maxWaterDepth = 50,
    turnRate = 300,

    canCloak = true,
    minCloakDistance = 100,

    reclaimable = true,
    capturable = true,

    autoHeal = 1.0,

    canAttack = true,
    canGuard = true,
    canFight = true,
    canPatrol = true,

    weapons = {
        [1] = {
            def = "light_laser",
        },
        [2] = {
            def = "rockets",
        },
    },

    sounds = {
        select = {
            "tone16.wav",
        },
        ok = {
            "newtarg1.wav",
        },
        arrived = {
            "tone2.wav",
        },
        cant = {
            "scold2.wav",
        },
    },
    
    explodeAs = "TANKDEATH",
}

local weaponDefs = {
    light_laser = {
	name = "Laser Beam",
	weaponType = "BeamLaser",
	avoidFeature = false,
	avoidFriendly = true,
	canAttackGround = true,
	collideFriendly = true,
	noselfDamage = true,
        turret = true,
	areaOfEffect = 2,
        reloadTime = 1.5,
	range = 550,
        targetMoveError = 0.5,
        tolerance = 128,
	--impulsefactor = 1,
	--intensity = 1,
        soundStart = "obelray1",
	beamTime = .2,
	rgbColor = "0.0 0.5 0.25",
        coreThickness = 1,
	rgbColor2 = "0.0 1.0 0.5",
	weaponVelocity = 900,
        --explosionGenerator = "custom:purplelaser",
        laserFlareSize = 2,
        thickness = 2,
        interceptedByShieldType = 1,
	damage = {
            default = 100,--180
        }
    },
    rockets = {
        name = "Missle",
        weaponType = "MissileLauncher",
	accuracy = 10,
	areaOfEffect = 100,
	avoidFeature = false,
	avoidFriendly = true,
	canAttackGround = true,
	collideFriendly = true,
        burst = 3,
	collisionSize = 8,
	commandFire = false,
	craterBoost = 0,
	craterMult = 0,
	edgeEffectiveness = 0.1,
	explosionSpeed = 128,
	impulseboost = 0,
	impulsefactor = 0,
	intensity = 1,
	noselfdamage = false,
        soundstart = "rocket1",
        soundhit = "xplos",
	range = 500,
	reloadtime = 10,
	rgbcolor = "1.0 1.0 1.0",
	turret = true,
	model = "projectiles/small_missile.dae",
        smokeTrail = true,
        texture2 = "spam_missile_trail",
        startVelocity = 400,
	weaponvelocity = 750,
        acceleration = 10.0,
        tracks = true,
        turnRate = 50000,
	explosiongenerator = "custom:TANKGUN_FX",
	damage = {
	    default = 25,
	},
    },
}

unitDef.weaponDefs = weaponDefs
return lowerkeys({ [unitName] = unitDef })
