
-- spam_config.lua

-- Spam default config

-- Spam relies on gadget delay api

return {
    max_units = 100, -- Maximum amount of units a ai instance will have at once
    wait = 300, -- Time before the ai begins its attack
    metal_income = 20, -- Its nominal metal income
    energy_income = 20, -- Its nominal energy income
    hq_build_speed = 5.0, --
    hq_hp = 1000000, -- Initial health of the spam hq
    hq_bonus_multiplier = 2, --
    hq_los = 512,
    hq_range = 1024, -- Range of all weapons
    hq_damage = 100, -- Damage for each of the 4 lasers with beamtime 0.25 and reload time 1.0
    unit_bonus_multiplier = 0.25,
    unit_lifetime = 300,

    build_order = {{"spam_light_tank", 100}, -- First entry is the unit name to be produced and the later is the likelyhood of it spawning
                   {"spam_medium_tank", 50},
                   {"spam_heavy_tank", 25},
    }
}
